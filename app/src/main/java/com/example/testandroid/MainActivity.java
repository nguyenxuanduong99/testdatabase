package com.example.testandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import com.example.testandroid.advanceddatabase.db.DatabaseAccess;

public class MainActivity extends AppCompatActivity {
    private DatabaseAccess databaseAccess;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.databaseAccess = DatabaseAccess.getInstance(getApplicationContext());
    }
    private void createDatabase() {
    SQLiteDatabase database = openOrCreateDatabase("contacts.db", Context.MODE_PRIVATE, null);
    database.execSQL("CREATE TABLE IF NOT EXISTS Contact(first_name TEXT, last_name TEXT, phone TEXT PRIMARY KEY, email TEXT);");
    database.close();
    }
}