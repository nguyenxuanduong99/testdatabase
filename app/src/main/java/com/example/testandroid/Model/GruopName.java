package com.example.testandroid.Model;

public class GruopName {
    int type_id;
    String group_name;
    String type_name;

    public GruopName(int type_id, String group_name, String type_name) {
        this.type_id = type_id;
        this.group_name = group_name;
        this.type_name = type_name;
    }

    public GruopName(String group_name, String type_name) {
        this.group_name = group_name;
        this.type_name = type_name;
    }

    public int getType_id() {
        return type_id;
    }

    public void setType_id(int type_id) {
        this.type_id = type_id;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public String getType_name() {
        return type_name;
    }

    public void setType_name(String type_name) {
        this.type_name = type_name;
    }
}
