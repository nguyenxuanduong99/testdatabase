package com.example.testandroid.Model;

public class Transaction {
    int id;
    int amount;
    String description;
    String time;
    String updatetime;
    int type_id;
    String group_name;

    public Transaction(int id, int amount, String description, String time, String updatetime, int type_id, String group_name) {
        this.id = id;
        this.amount = amount;
        this.description = description;
        this.time = time;
        this.updatetime = updatetime;
        this.type_id = type_id;
        this.group_name = group_name;
    }

    public Transaction() {
        this.amount = amount;
        this.description = description;
        this.time = time;
        this.updatetime = updatetime;
        this.type_id = type_id;
        this.group_name = group_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public int getType_id() {
        return type_id;
    }

    public void setType_id(int type_id) {
        this.type_id = type_id;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }
}
