package com.example.testandroid.advanceddatabase.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.testandroid.Model.Transaction;

import java.util.ArrayList;
import java.util.List;

public class DatabaseAccess {
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase sqLiteDatabase;
    private static  DatabaseAccess instance;
    private DatabaseAccess(Context context){
        this.openHelper = new Database(context);
    }
    public static  DatabaseAccess getInstance(Context context){
        if(instance ==null){
            instance = new DatabaseAccess(context);
        }
        return instance;
    }
    public void open(){
        this.sqLiteDatabase= openHelper.getWritableDatabase();
    }
    public void close(){
        if(sqLiteDatabase !=null){
            this.sqLiteDatabase.close();
        }
    }
    public void insertContact(Transaction contact) {
        ContentValues values = new ContentValues();
        values.put("inc_amount", contact.getAmount());
        values.put("inc_description", contact.getDescription());
        values.put("inc_time", contact.getTime());
        values.put("inc_group_name", contact.getGroup_name());
        values.put("inc_type_id",contact.getType_id());
        sqLiteDatabase.insert("Contact", null, values);
    }


    public List<Transaction> getContacts() {
        List<Transaction> list = new ArrayList<>();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM Contact", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Transaction contact = new Transaction();
            contact.setAmount(cursor.getInt(0));
            contact.setDescription(cursor.getString(1));
            contact.setTime(cursor.getString(2));
            contact.setType_id(cursor.getInt(3));
            contact.setGroup_name(cursor.getString(4));
            list.add(contact);
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }


    public void updateTransaction(Transaction contact) {
        ContentValues values = new ContentValues();
        values.put("inc_amount", contact.getAmount());
        values.put("inc_description", contact.getDescription());
        values.put("inc_time", contact.getTime());
        values.put("inc_group_name", contact.getGroup_name());
        values.put("inc_type_id",contact.getType_id());
        sqLiteDatabase.update("incomes",values,"inc_id= ?",new String[]{String.valueOf(contact.getId())});
    }

    /**
     * Delete the provided contact.
     *
     * @param contact the contact to delete
     */
    public void deleteContact(Transaction contact) {
        sqLiteDatabase.delete("incomes", "inc_id = ?", new String[]{String.valueOf(contact.getId())});
        sqLiteDatabase.close();
    }
}
